package com.apps.srashtasoft.autoclickify.services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.PixelFormat
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager

import com.apps.srashtasoft.autoclickify.R
import dp2px
import kotlinx.android.synthetic.main.floating_menu_layout.view.*
import logd

class FlotingMenuService : Service()  {

    private var serviceIntent: Intent? = null
    private lateinit var manager: WindowManager
    private lateinit var view: View
    private lateinit var view1: View
    private lateinit var secoundClick: View
    private lateinit var params: WindowManager.LayoutParams
    private lateinit var params1: WindowManager.LayoutParams
    private lateinit var params2: WindowManager.LayoutParams
    private var xForRecord = 0
    private var yForRecord = 0
    private val location = IntArray(2)
    private var startDragDistance: Int = 0
    private var clickSec : Long = 1000
    private var isLoop : Boolean = false
    private var isPlay : Boolean = false
    private var clickSecTag : Boolean = true

    private var isOneClick : Boolean = true
    private var timer : CountDownTimer? = null
    private var clickCount : Int = 0
    private var clickCounter : Int = 1





    private fun makeClickAction(){
        println("#### makeClickAction")
        if(isOneClick){
            callOneNumberClick()
        }else{
            if(clickSecTag){
                clickSecTag = false
                callOneNumberClick()
            }else{
                clickSecTag = true
                callSecoundNumberClick()
            }
        }

    }
    private fun callOneNumberClick(){
        println("## callOneNumberClick")
        view1.getLocationOnScreen(location)
        autoClickService?.click(location[0] + view1.right ,
            location[1] + view1.bottom )
        timer!!.cancel()
        timer!!.start()
    }
    private fun callSecoundNumberClick(){
        secoundClick.getLocationOnScreen(location)
        autoClickService?.click(location[0] + secoundClick.right ,
            location[1] + secoundClick.bottom )
        timer!!.cancel()
        timer!!.start()
    }


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
      var temp = intent?.getIntExtra("CLICK_SEC",1)
        clickCount = intent?.getIntExtra("CLICK_COUNT",1)!!
        isLoop =intent?.getBooleanExtra("IS_LOOP",false)!!
        isOneClick =intent?.getBooleanExtra("IS_ONE_CLICK",true)!!
        clickSec = temp!!.toLong() * 1000
        println("## Timer Secound $clickSec")
        println("## Auto Clicks $clickCount")
        println("## How Many  Clicks $isOneClick")
        if(!isOneClick)
            manager.addView(secoundClick,params2)


         timer = object : CountDownTimer(clickSec,1000){
            override fun onFinish() {
                println("## Click Calling")
                if(!isLoop){
                    clickCounter++
                    if(clickCounter > clickCount){
                        println("#### Timer cancel")
                        isPlay = false
                        view.xStartClick.setImageDrawable(getDrawable(R.drawable.ic_play))
                        timer!!.cancel()
                    }else{

                        makeClickAction()
                    }
                }else{
                    makeClickAction()
                }



            }

            override fun onTick(p0: Long) {

            }
        }
        return super.onStartCommand(intent, flags, startId)

    }


    override fun onCreate() {
        super.onCreate()
        println("## onCreate Method")
        startDragDistance = dp2px(10f)
        view = LayoutInflater.from(this).inflate(R.layout.floating_menu_layout, null)
        view1 = LayoutInflater.from(this).inflate(R.layout.one_click_layout, null)
        secoundClick = LayoutInflater.from(this).inflate(R.layout.secound_click_layout, null)
        val overlayParam =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
                WindowManager.LayoutParams.TYPE_PHONE
            }
        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
        params1 = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
        params2 = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
        params1.gravity = Gravity.CENTER
        params.gravity = Gravity.LEFT
        //params2 = params1
        params2.gravity = Gravity.TOP

        manager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        manager.addView(view, params)
        manager.addView(view1,params1)
        view.xclick1.setOnClickListener {
            println("## One Click ")
            if(!isPlay){
                isPlay = true
                view.xStartClick.setImageDrawable(getDrawable(R.drawable.ic_stop))
                timer?.start()
                clickCounter = 0
            }else{
                isPlay = false
                view.xStartClick.setImageDrawable(getDrawable(R.drawable.ic_play))
                timer?.cancel()
            }

        }
       /* view.xSwap.setOnTouchListener(
            TouchAndDragListener(params, startDragDistance,
                { viewOnClick() },
                { manager.updateViewLayout(view, params) })
        )
        view1.setOnTouchListener(
            TouchAndDragListener(params1, startDragDistance,
                { viewOnClick() },
                { manager.updateViewLayout(view1, params1) })
        )
        secoundClick.setOnTouchListener(
            TouchAndDragListener(params2, startDragDistance,
                { viewOnClick() },
                { manager.updateViewLayout(secoundClick, params2) })
        )*/
    }

    private fun viewOnClick() {


    }

    override fun onDestroy() {
        super.onDestroy()
        "FloatingClickService onDestroy".logd()

        manager.removeView(view)
        manager.removeView(view1)

        if(!isOneClick)
            manager.removeView(secoundClick)
    }
    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        "FloatingClickService onConfigurationChanged".logd()
        val x = params.x
        val y = params.y
        params.x = xForRecord
        params.y = yForRecord
        xForRecord = x
        yForRecord = y
        manager.updateViewLayout(view, params)
        manager.updateViewLayout(view1,params)

        if(!isOneClick)
            manager.updateViewLayout(secoundClick,params)

    }
}