package com.apps.srashtasoft.autoclickify.services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.PixelFormat
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import com.apps.srashtasoft.autoclickify.R
import dp2px
import logd

class oneClickWidget : Service() {

    private var serviceIntent: Intent? = null
    private lateinit var manager: WindowManager
    private lateinit var view: View
    private lateinit var params: WindowManager.LayoutParams
    private var xForRecord = 0
    private var yForRecord = 0
    private val location = IntArray(2)
    private var startDragDistance: Int = 0


    private var timer = object : CountDownTimer(10000,1000){
        override fun onFinish() {
            println("## Click Calling")
            /*view.getLocationOnScreen(location)
            autoClickService?.click(location[0] + view.right + 10,
                location[1] + view.bottom + 10)*/
        }

        override fun onTick(p0: Long) {

        }
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        startDragDistance = dp2px(10f)
        view = LayoutInflater.from(this).inflate(R.layout.one_click_layout, null)
        val overlayParam =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
                WindowManager.LayoutParams.TYPE_PHONE
            }
        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
       // params.gravity = Gravity.TOP
      //  params.gravity = Gravity.CENTER
        manager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        manager.addView(view, params)

       /* view.click1.setOnTouchListener(
            TouchAndDragListener(params, startDragDistance,
                { viewOnClick() },
                { manager.updateViewLayout(view, params) })
        )*/


    }
    private fun viewOnClick() {
        //timer
    }

     fun clicked(){
        println("## clicked")
         timer.start()

    }

    override fun onDestroy() {
        super.onDestroy()
        "FloatingClickService onDestroy".logd()
        timer?.cancel()
        manager.removeView(view)
    }
    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        "FloatingClickService onConfigurationChanged".logd()
        val x = params.x
        val y = params.y
        params.x = xForRecord
        params.y = yForRecord
        xForRecord = x
        yForRecord = y
        manager.updateViewLayout(view, params)
    }
}