package com.apps.srashtasoft.autoclickify.fragments

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast

import com.apps.srashtasoft.autoclickify.R
import com.apps.srashtasoft.autoclickify.extras.SharePrefrenceValue
import kotlinx.android.synthetic.main.single_target_setting_frg.view.*

var list_of_items = arrayOf("Second(s)","Minute(s)")
class SingleTargetSettingFrg(var isSingle : Boolean) : Fragment() {

    var mView: View? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.single_target_setting_frg, container, false)

        val aa = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, list_of_items)

        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        mView!!.xSpinner.adapter = aa


        mView?.xRbtInfinite?.setOnClickListener {

            mView?.xRbtAmountCycle?.isChecked = false
            mView?.xRbtAmountTime?.isChecked = false
        }
        mView?.xRbtAmountCycle?.setOnClickListener {

            mView?.xRbtInfinite?.isChecked = false
            mView?.xRbtAmountTime?.isChecked = false
        }
        mView?.xRbtAmountTime?.setOnClickListener {

            mView?.xRbtInfinite?.isChecked = false
            mView?.xRbtAmountCycle?.isChecked = false
        }

        oparationFunction()

        bindDataSingleTarget()


        return mView
    }



    private fun bindDataSingleTarget(){

        mView?.xSpinner!!.setSelection(SharePrefrenceValue().getClickIntervalType(context!!,isSingle))
        mView?.xEdtIntervalValue!!.setText(""+SharePrefrenceValue().getClickIntervalValue(context!!,isSingle))
        var stopType : Int = SharePrefrenceValue().getClickStopType(context!!,isSingle)
        if(stopType == 1){
            mView?.xRbtInfinite?.isChecked = true
        }else{
            mView?.xRbtAmountCycle?.isChecked = true
            mView?.xEdtCycleValue!!.setText(""+SharePrefrenceValue().getClickCycleValue(context!!,isSingle))
        }
    }


    private fun oparationFunction(){




        mView?.xCvSetSetting!!.setOnClickListener {

            SharePrefrenceValue().setClickIntervalType(context!!, mView?.xSpinner!!.selectedItemPosition,isSingle)
                if(mView?.xEdtIntervalValue!!.text.trim().isEmpty()){
                    Toast.makeText(context!!,"Enter click interval value",Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }else if(mView?.xRbtAmountCycle!!.isChecked){
                    if(mView?.xEdtCycleValue!!.text.trim().isEmpty()){
                        Toast.makeText(context!!,"Enter amount of cycle",Toast.LENGTH_LONG).show()
                        return@setOnClickListener
                    }else{
                        SharePrefrenceValue().setClickStopType(context!!,2,isSingle)
                        SharePrefrenceValue().setClickIntervalValue(context!!,mView?.xEdtIntervalValue!!.text.toString(),isSingle)
                        SharePrefrenceValue().setClickCycleValue(context!!,mView?.xEdtCycleValue!!.text.toString().toInt(),isSingle)
                        (context!! as Activity).finish()
                        // save data
                    }
                }else{
                    SharePrefrenceValue().setClickStopType(context!!,1,isSingle)
                    SharePrefrenceValue().setClickIntervalValue(context!!,mView?.xEdtIntervalValue!!.text.toString(),isSingle)
                    //save data
                    (context!! as Activity).finish()
                }
        }


    }








}
