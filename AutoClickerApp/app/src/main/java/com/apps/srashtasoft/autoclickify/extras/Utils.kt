package com.apps.srashtasoft.autoclickify.extras

import android.accessibilityservice.AccessibilityServiceInfo

import android.app.Activity
import android.app.ActivityManager
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri

import android.view.accessibility.AccessibilityManager

import com.apps.srashtasoft.autoclickify.R
import com.apps.srashtasoft.autoclickify.services.FlotingMenuService

class Utils {

    fun isMyServiceRunning(calssObj: Class<FlotingMenuService>, activity : Activity): Boolean {
        val manager = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (calssObj.getName().equals(service.service.getClassName())) {
                return true
            }
        }
        return false
    }

     fun checkAccess(context : Context): Boolean {
        val string = context.getString(R.string.accessibility_service_id)
        val manager = context.getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
        val list = manager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_GENERIC)
        for (id in list) {
            if (string == id.id) {
                return true
            }
        }
        return false
    }

    fun getClickInterval(context: Context,isSingleMode : Boolean): Long{
        var clickIntervalType : Int = SharePrefrenceValue().getClickIntervalType(context,isSingleMode)
        var IntervalValue : Long = SharePrefrenceValue().getClickIntervalValue(context,isSingleMode)
       // if(clickIntervalType == 0){
          //  return IntervalValue
        //}else
            if(clickIntervalType == 0){
            return IntervalValue*1000
        }else{
            return IntervalValue * 60 * 1000
        }
    }
    fun rateUs(context : Context){
        var uri = Uri.parse("https://play.google.com/store/apps/details?id="+context.packageName+"&hl=en")
        var goToMarket = Intent(Intent.ACTION_VIEW,uri)
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
        Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
        Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try{
            context.startActivity(goToMarket)
        }catch (e : ActivityNotFoundException){
        context.startActivity( Intent(Intent.ACTION_VIEW,
           Uri.parse("https://play.google.com/store/apps/details?id="+context.packageName) ))
        }

    }

    fun shareApp(context: Context){
        var shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Auto Clicker App")
        var shareMessage= "Let me recommend you this application\n"
        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + context.packageName+"\n\n";
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        context.startActivity(Intent.createChooser(shareIntent, "choose one"));

    }
    fun showCreditDialog(context: Context){
        var builder = AlertDialog.Builder(context)
        var dialog : AlertDialog = builder.create()

        dialog.setButton("Ok"){
                dialogInterface, i ->
            dialog.dismiss()
        }

        dialog.setTitle("Credits")
        dialog.setMessage("App Developed By \n\nApp icons and image made by Srashtasoft Technologies www.srashtasoft.com \n")
        dialog.show()
    }

    fun showPolicy(context: Context){
        var browserIntent =  Intent(Intent.ACTION_VIEW,
            Uri.parse("https://developer.srashtasoft.com/projects/socialapp-admin/SocialApp/privacy.php"));
        context.startActivity(browserIntent);
    }
    fun sendFeedBck(context: Context){
        var sendEmail = Intent(Intent.ACTION_SEND)
        sendEmail.setType("text/html");
        sendEmail.putExtra(Intent.EXTRA_EMAIL, arrayOf("info@srashtasoft.com"))
        //sendEmail.putExtra(Intent.EXTRA_EMAIL, "info@srashtasoft.com")
        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "FeedBack :: Auto Clicker App")
        sendEmail.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        sendEmail.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        context.startActivity(Intent.createChooser(sendEmail, "Send Email"));

    }

    fun  getTextSize(size : Int): Float{
        if(size > 70 && size < 80){
            return 21f
        }else if(size > 80 && size < 90){
            return  25f
        }else if(size > 90 && size < 110){
            return 30f
        }
        else if(size > 110 && size < 130){
            return  36f
        }
        else if(size > 130 && size < 150){
            return  40f
        }
        else if(size > 150 && size < 170){
            return  45f
        }
        return 24f
    }


}