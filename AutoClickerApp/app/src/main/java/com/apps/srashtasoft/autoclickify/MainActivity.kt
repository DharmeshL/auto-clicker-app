package com.apps.srashtasoft.autoclickify

import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.apps.srashtasoft.autoclickify.activities.*
import com.apps.srashtasoft.autoclickify.extras.Utils
import com.apps.srashtasoft.autoclickify.services.*

import kotlinx.android.synthetic.main.malti_target_card_layout.*
import kotlinx.android.synthetic.main.single_target_card_layout.*
import java.lang.Exception


private const val PERMISSION_CODE = 110

class MainActivity : AppCompatActivity() {


    private var serviceIntent: Intent? = null
    private var oneClickServiceIntent: Intent? = null
    val reciever :BroadcastReceiver? = ServiceStop()
    val multiClickreciever :BroadcastReceiver? = MultiServiceStop()
    private var oneClickServiceRunning : Boolean = false
    private var multiClickServiceRunning : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }

    fun onClick(view : View){
        if(view.id == null){
            return
        }
    try{
        when(view.id){
            R.id.xLlSingleSetting ->{
                if(!oneClickServiceRunning){
                    var i = Intent(this@MainActivity,SettingModeActivity::class.java)
                    i.putExtra("IS_SINGLE_TARGET",true)
                    startActivity(i)

                }

            }
            R.id.xllMultiSettingMode ->{
                if(!multiClickServiceRunning){
                    var i = Intent(this@MainActivity,SettingModeActivity::class.java)
                    i.putExtra("IS_SINGLE_TARGET",false)
                    startActivity(i)
                }

            }

            R.id.xCvStartSingleService ->{
                // xCvStartSingleService.cardBackgroundColor = resources.getColor(R.color.colorGray)
                if(multiClickServiceRunning){
                    multiClickServiceRunning = false
                    stopAndStartMaltiService(multiClickServiceRunning)
                    oneClickServiceRunning = true
                    startAndStopSingleService(true)
                }else if(oneClickServiceRunning){
                    startAndStopSingleService(false)
                    oneClickServiceRunning = false

                }else{
                    startAndStopSingleService(true)
                    oneClickServiceRunning = true
                }

            }
            R.id.xCvMultiTarget -> {
                if(multiClickServiceRunning){
                    multiClickServiceRunning = false
                    stopAndStartMaltiService(multiClickServiceRunning)

                }else if(oneClickServiceRunning){
                    oneClickServiceRunning = false
                    startAndStopSingleService(false)
                    multiClickServiceRunning = true
                    stopAndStartMaltiService(multiClickServiceRunning)
                }else{
                    multiClickServiceRunning = true
                    stopAndStartMaltiService(multiClickServiceRunning)
                }
            }

            R.id.xLlCommonSetting -> {
                if(oneClickServiceRunning == false && multiClickServiceRunning == false){
                    startActivity(Intent(this@MainActivity,OtherSettingsActivity::class.java))
                }
            }
            R.id.xllSInstruction -> {
                startActivity(Intent(this@MainActivity, SingleInstructionActivity::class.java))
            }
            R.id.xllMInstruction -> {

                startActivity(Intent(this@MainActivity, MultiInstructionActivity::class.java))
            }
            R.id.xLlTroubleshooting -> {

                startActivity(Intent(this@MainActivity, TroubleshootingActivity::class.java))


            }

        }
    }catch (e : Exception) {

    }



    }

    private fun startAndStopSingleService(tag : Boolean){
        if(tag){
            xCvStartSingleService.setCardBackgroundColor(resources.getColor(R.color.colorGray))
            xTvSStart.text = "DISABLE"
            startService(Intent(this@MainActivity,SingleTargetModeService::class.java))
        }else{
            stopService(Intent(this@MainActivity,SingleTargetModeService::class.java))
            xCvStartSingleService.setCardBackgroundColor(resources.getColor(R.color.colorAccent))
            xTvSStart.text = "ENABLE"
        }

    }

    private fun stopAndStartMaltiService(tag : Boolean){
        if(tag){
            xCvMultiTarget.setCardBackgroundColor(resources.getColor(R.color.colorGray))
            xTvMStart.text = "DISABLE"
            startService(Intent(this@MainActivity,MaltiTargetModeService::class.java))
        }else{
            xCvMultiTarget.setCardBackgroundColor(resources.getColor(R.color.colorAccent))
            xTvMStart.text = "ENABLE"
            stopService(Intent(this@MainActivity,MaltiTargetModeService::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(reciever,getIntentFilter(false))
        registerReceiver(multiClickreciever,getIntentFilter(true))

    }


    override fun onResume() {
        super.onResume()
        val hasPermission = Utils().checkAccess(this)
    //    "has access? $hasPermission".logd()

        if (!hasPermission) {
            startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            && !Settings.canDrawOverlays(this)) {
            askPermission()
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    private fun askPermission() {
        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
            Uri.parse("package:$packageName"))
        startActivityForResult(intent, PERMISSION_CODE)
    }
    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(reciever)
        unregisterReceiver(multiClickreciever)
        serviceIntent?.let {
            //"stop floating click service".logd()
            stopService(it)
        }
        autoClickService?.let {
            //"stop auto click service".logd()
            it.stopSelf()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) return it.disableSelf()
            autoClickService = null
        }

    }
    override fun onBackPressed() {
        moveTaskToBack(true)
    }
    private fun getIntentFilter(isMulti : Boolean): IntentFilter {
        val iFilter = IntentFilter()
        if(isMulti)
            iFilter.addAction("STOP_MULTI_TARGET")
        else
            iFilter.addAction("STOP")



        return iFilter
    }

    inner class ServiceStop : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            xCvStartSingleService.setCardBackgroundColor(resources.getColor(R.color.colorAccent))
            xTvSStart.text = "ENABLE"
            oneClickServiceRunning = false
        }

    }

    inner class MultiServiceStop : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            multiClickServiceRunning = false
            stopAndStartMaltiService(multiClickServiceRunning)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu,menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_rate_app ->{
                Utils().rateUs(this)
            }
            R.id.nav_share ->{
                Utils().shareApp(this)
            }
            R.id.nav_feedback ->{
                Utils().sendFeedBck(this)
            }
            R.id.nav_credit ->{
                Utils().showCreditDialog(this)
            }
            R.id.nav_policy ->{
                Utils().showPolicy(this)
            }
            R.id.nav_ads ->{

            }
        }
        return super.onOptionsItemSelected(item)
    }
}
