package com.apps.srashtasoft.autoclickify.dialogs

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.apps.srashtasoft.autoclickify.R
import android.view.WindowManager

import android.os.Build
import android.widget.*
import androidx.cardview.widget.CardView
import com.apps.srashtasoft.autoclickify.extras.SharePrefrenceValue
import com.apps.srashtasoft.autoclickify.fragments.list_of_items


class SingleModeSettingDailog {
    fun showDialog(context : Context,isSingle : Boolean){
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.single_target_setting_frg)
        //dialog.getWindow()!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            dialog.getWindow()!!.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY)
        }else{
            dialog.getWindow()!!.setType(WindowManager.LayoutParams.TYPE_TOAST)
        }




        var xRbtInfinite = dialog.findViewById(R.id.xRbtInfinite) as RadioButton
        var xRbtAmountTime = dialog.findViewById(R.id.xRbtAmountTime) as RadioButton
        var xRbtAmountCycle = dialog.findViewById(R.id.xRbtAmountCycle) as RadioButton
        var xSpinner = dialog.findViewById(R.id.xSpinner) as Spinner
        var xSave = dialog.findViewById(R.id.xCvSetSetting)as CardView
        var xEdtIntervalValue = dialog.findViewById(R.id.xEdtIntervalValue)as EditText
        var xEdtCycleValue = dialog.findViewById(R.id.xEdtCycleValue)as EditText


      //  xRbtInfinite.isChecked = true
        val aa = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, list_of_items)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        xSpinner.adapter = aa

        // Bind Data
        xSpinner!!.setSelection(SharePrefrenceValue().getClickIntervalType(context!!,isSingle))
        xEdtIntervalValue!!.setText(""+SharePrefrenceValue().getClickIntervalValue(context!!,isSingle))
        var stopType : Int = SharePrefrenceValue().getClickStopType(context!!,isSingle)
        if(stopType == 1){
            xRbtInfinite?.isChecked = true
        }else{
            xRbtAmountCycle?.isChecked = true
            xEdtCycleValue!!.setText(""+SharePrefrenceValue().getClickCycleValue(context!!,isSingle))
        }

        xRbtInfinite?.setOnClickListener {

            xRbtAmountCycle?.isChecked = false
            xRbtAmountTime?.isChecked = false
        }
        xRbtAmountCycle?.setOnClickListener {

            xRbtInfinite?.isChecked = false
            xRbtAmountTime?.isChecked = false
        }
        xRbtAmountTime?.setOnClickListener {

            xRbtInfinite?.isChecked = false
            xRbtAmountCycle?.isChecked = false
        }
        xSave.setOnClickListener {
            SharePrefrenceValue().setClickIntervalType(context!!,xSpinner!!.selectedItemPosition,isSingle)
            if(xEdtIntervalValue!!.text.trim().isEmpty()){
                Toast.makeText(context!!,"Enter click interval value",Toast.LENGTH_LONG).show()
                return@setOnClickListener

            }
            else if(xRbtAmountCycle!!.isChecked){
                if(xEdtCycleValue!!.text.trim().isEmpty()){
                    Toast.makeText(context!!,"Enter amount of cycle",Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }else{
                    SharePrefrenceValue().setClickStopType(context!!,2,isSingle)
                    SharePrefrenceValue().setClickIntervalValue(context!!,xEdtIntervalValue!!.text.toString(),isSingle)
                    SharePrefrenceValue().setClickCycleValue(context!!,xEdtCycleValue!!.text.toString().toInt(),isSingle)
                    dialog.dismiss()
                    // save data
                }
            }else{
                SharePrefrenceValue().setClickStopType(context!!,1,isSingle)
                SharePrefrenceValue().setClickIntervalValue(context!!,xEdtIntervalValue!!.text.toString(),isSingle)
                //save data
                dialog.dismiss()

            }
        }


        dialog.show()

    }
}