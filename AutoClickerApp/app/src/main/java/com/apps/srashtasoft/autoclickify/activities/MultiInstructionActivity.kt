package com.apps.srashtasoft.autoclickify.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.apps.srashtasoft.autoclickify.R

class MultiInstructionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_instruction)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle(R.string.multiTargetMode)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{

                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
