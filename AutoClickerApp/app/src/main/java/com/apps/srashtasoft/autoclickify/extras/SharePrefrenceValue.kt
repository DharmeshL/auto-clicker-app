package com.apps.srashtasoft.autoclickify.extras

import android.content.Context
import android.content.SharedPreferences

class SharePrefrenceValue {


    // This for Single Mode Target
    fun setClickIntervalType(context: Context, flag: Int,isSingle : Boolean) {
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        if(isSingle)
        editor.putInt("INTERVAL_TYPE", flag)
        else
            editor.putInt("INTERVAL_TYPE_M", flag)
        editor.commit()
    }

    fun setClickIntervalValue(context: Context, value: String,isSingle : Boolean) {
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        if(isSingle)
        editor.putLong("INTERVAL_VALUE", value.toLong())
        else
            editor.putLong("INTERVAL_VALUE_M", value.toLong())

        editor.commit()
    }

    fun getClickIntervalType(context: Context,isSingle : Boolean): Int {
        // 0 For Millisecound , 1 for secound and 2 for Minute
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        if(isSingle)
        return preferences.getInt("INTERVAL_TYPE", 0)
        else
            return preferences.getInt("INTERVAL_TYPE_M", 0)

    }
    fun getClickIntervalValue(context: Context,isSingle : Boolean): Long {

        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)

        if(isSingle)
        return preferences.getLong("INTERVAL_VALUE", 1L)
        else
            return preferences.getLong("INTERVAL_VALUE_M", 1L)

    }

    fun setClickStopType(context: Context, value: Int,isSingle : Boolean) {
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        if(isSingle)
        editor.putInt("STOP_TYPE", value)
        else
            editor.putInt("STOP_TYPE_M", value)
        editor.commit()
    }

    fun getClickStopType(context: Context,isSingle : Boolean): Int {
        // 1 For indefinetelly , 2 Amount of Cycle
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        if(isSingle)
        return preferences.getInt("STOP_TYPE", 1)
        else
            return preferences.getInt("STOP_TYPE_M", 1)

    }

    fun setClickCycleValue(context: Context, value: Int,isSingle : Boolean) {
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        if(isSingle)
        editor.putInt("CLICK_CYCLE_VALUE", value)
        else
            editor.putInt("CLICK_CYCLE_VALUE_M", value)

        editor.commit()
    }

    fun getClickCycleValue(context: Context,isSingle : Boolean): Int {
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        if(isSingle)
        return preferences.getInt("CLICK_CYCLE_VALUE", 10)
        else
            return preferences.getInt("CLICK_CYCLE_VALUE_M", 10)

    }

    fun setTargetValue(context: Context, value: Int){
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        editor.putInt("TARGET_SIZE", value)
        editor.commit()
    }



    fun getTargetValue(context: Context): Int {
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)

            return preferences.getInt("TARGET_SIZE", 70)


    }

    fun setControllerSize(context: Context, value: Int){
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        editor.putInt("CONTROLLER_SIZE", value)
        editor.commit()
    }
    fun getControllerSize(context: Context): Int {
        val preferences: SharedPreferences =
            context!!.applicationContext.getSharedPreferences("APP_DATA", Context.MODE_PRIVATE)

        return preferences.getInt("CONTROLLER_SIZE", 1)


    }






}