package com.apps.srashtasoft.autoclickify.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AlphaAnimation
import com.apps.srashtasoft.autoclickify.R
import kotlinx.android.synthetic.main.single_target_card_layout.*

class SingleInstructionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_instruction)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle(R.string.singleTargetMode)
        xFliking.visibility = View.VISIBLE
        val anim = AlphaAnimation(1.0f, 0.2f)
        anim.duration = 500
        anim.fillAfter = true

// here is repeat settings
        anim.repeatMode = AlphaAnimation.REVERSE // ping pong mode
        anim.repeatCount = AlphaAnimation.INFINITE // count of repeats

        xFliking.startAnimation(anim)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{

                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
