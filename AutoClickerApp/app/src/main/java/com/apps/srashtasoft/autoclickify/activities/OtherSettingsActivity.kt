package com.apps.srashtasoft.autoclickify.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SeekBar
import com.apps.srashtasoft.autoclickify.R
import com.apps.srashtasoft.autoclickify.extras.SharePrefrenceValue
import kotlinx.android.synthetic.main.activity_other_settings.*

class OtherSettingsActivity : AppCompatActivity() {


    var size : Int = 100
    var cSize : Int = 60
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_other_settings)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle(R.string.common_setting)
        size =SharePrefrenceValue().getTargetValue(this)
        cSize =SharePrefrenceValue().getControllerSize(this)
        xControlSeekBar.setProgress(cSize)

        setData(cSize)
        println("## Size 12 $size")
        xSeekBar.setProgress(size-70)

        xImgTarget.layoutParams.width = size
        xImgTarget.layoutParams.height = size
        xImgTarget.requestLayout()
     //   changeSize(cSize)




        xSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                size = p1+70
                xImgTarget.layoutParams.width = size
                xImgTarget.layoutParams.height = size
                xImgTarget.requestLayout()

                println("## Size $size")


            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        })

      //var test =   this.resources.getDimension(R.dimen.small_controller)
        xControlSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {

                setData(p1)

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        })
    }

    private fun setData(p1 : Int){
        var scale : Float = this.resources.displayMetrics.density
        var pixel : Float = 0f
        if(p1 == 0){
            pixel  = (30 * scale + 0.5f)

        }else if(p1 == 1){
            pixel  = (40 * scale + 0.5f)
        }else{
            pixel  = (50 * scale + 0.5f)
        }
        changeSize(pixel.toInt())
        cSize = p1
    }

    private fun changeSize(fSize : Int){
        xImgPlayButton.layoutParams.width = fSize
        xImgPlayButton.layoutParams.height = fSize
        xImgPlayButton.requestLayout()

        xImgAddClick.layoutParams.width = fSize
        xImgAddClick.layoutParams.height = fSize


        xImgRemoveClick.layoutParams.width = fSize
        xImgRemoveClick.layoutParams.height = fSize


        xImgSetting.layoutParams.width = fSize
        xImgSetting.layoutParams.height = fSize


        xImgClose.layoutParams.width = fSize
        xImgClose.layoutParams.height = fSize


        xImgMove.layoutParams.width = fSize
        xImgMove.layoutParams.height = fSize

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{

               onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        SharePrefrenceValue().setTargetValue(this,size)
        SharePrefrenceValue().setControllerSize(this,cSize)
        finish()
    }
}
