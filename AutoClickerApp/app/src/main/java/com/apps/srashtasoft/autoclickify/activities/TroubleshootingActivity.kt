package com.apps.srashtasoft.autoclickify.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import com.apps.srashtasoft.autoclickify.R
import kotlinx.android.synthetic.main.activity_troubleshooting.*

class TroubleshootingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_troubleshooting)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle(R.string.troubleshooting)

        xCvReActive.setOnClickListener {
            startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))
        }


        xCvAllow.setOnClickListener {
            try{
                startActivity(Intent(Settings.ACTION_APPLICATION_SETTINGS))
            }catch (e : Exception){
                e.printStackTrace()
            }

        }




    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{

                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
