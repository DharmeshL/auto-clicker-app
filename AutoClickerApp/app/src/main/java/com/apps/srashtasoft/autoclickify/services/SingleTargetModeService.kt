package com.apps.srashtasoft.autoclickify.services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.PixelFormat
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.view.*
import android.widget.ImageView
import android.widget.Toast

import com.apps.srashtasoft.autoclickify.R
import com.apps.srashtasoft.autoclickify.dialogs.SingleModeSettingDailog
import com.apps.srashtasoft.autoclickify.extras.SharePrefrenceValue
import com.apps.srashtasoft.autoclickify.extras.Utils
import dp2px
import com.apps.srashtasoft.autoclickify.listners.TouchAndDragListener
import com.apps.srashtasoft.autoclickify.listners.freez
import kotlinx.android.synthetic.main.single_target_click.view.*
import logd
import java.lang.Exception

class SingleTargetModeService : Service()  {


    private lateinit var manager: WindowManager
    private lateinit var view: View
    private lateinit var clickView: View
    private lateinit var params: WindowManager.LayoutParams
    private lateinit var clickParam: WindowManager.LayoutParams
    private var xForRecord = 0
    private var yForRecord = 0
    private val location = IntArray(2)
    private var startDragDistance: Int = 0
    private var timer : CountDownTimer? = null
    private var isStart : Boolean = false
    private var isExpanded : Boolean = true
    private var clickInterval : Long =1000L
    private var isLoop : Int = 1
    private var clickCycle : Int = 1






    private fun callOneNumberClick(){
        println("## callOneNumberClick")
        clickView.getLocationOnScreen(location)
        autoClickService?.click(location[0] + clickView.right - 30,
            location[1] + clickView.bottom )
     //   clickView.callOnClick();
        timer!!.cancel()
        if(isLoop == 1){
            timer!!.start()
        }else{
            if(clickCycle > 1){
                clickCycle--
                timer!!.start()
            }else{
               stopTimer()
            }
        }
       // timer!!.start()
    }





    override fun onBind(p0: Intent?): IBinder? {
        return null
    }


    private fun setConrollerSize(cSize : Int){
        var xImgClose = view.findViewById(R.id.xImgClose) as ImageView
        var xImgPlay = view.findViewById(R.id.xImgPlay) as ImageView
        var xImgSetting = view.findViewById(R.id.xImgSetting) as ImageView
        var xImgMove = view.findViewById(R.id.xImgMove) as ImageView
        var scale : Float = this.resources.displayMetrics.density
        var pixel : Float = (cSize * scale + 0.5f)
        xImgClose.layoutParams.width = pixel.toInt()
        xImgClose.layoutParams.height = pixel.toInt()
        xImgPlay.layoutParams.width = pixel.toInt()
        xImgPlay.layoutParams.height = pixel.toInt()
        xImgSetting.layoutParams.width = pixel.toInt()
        xImgSetting.layoutParams.height = pixel.toInt()
        xImgMove.layoutParams.width = pixel.toInt()
        xImgMove.layoutParams.height = pixel.toInt()
    }



    override fun onCreate() {
        super.onCreate()
        println("## onCreate Method")
        startDragDistance = dp2px(10f)
        view = LayoutInflater.from(this).inflate(R.layout.single_target_click, null)
        clickView = LayoutInflater.from(this).inflate(R.layout.one_click_layout, null)
        var xImgTarget = clickView.findViewById(R.id.xIvTarget) as ImageView

        var size = SharePrefrenceValue().getTargetValue(this)
        var Csize = SharePrefrenceValue().getControllerSize(this)
        if(Csize == 0){
            setConrollerSize(30)
        }else if(Csize == 1){
            setConrollerSize(40)
        }else{
            setConrollerSize(50)
        }


                xImgTarget.layoutParams.width = size
              xImgTarget.layoutParams.height = size

        val overlayParam =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
                WindowManager.LayoutParams.TYPE_PHONE
            }
        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
        clickParam = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)



        params.gravity = Gravity.LEFT

        manager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        manager.addView(view, params)
        manager.addView(clickView, clickParam)




        view.xImgMove.setOnTouchListener(
            TouchAndDragListener(params, startDragDistance,
                { viewOnClick() },
                { manager.updateViewLayout(view, params) },true)
        )
        view.xImgPlay.setOnClickListener {
            if(!isStart){
                clickInterval = Utils().getClickInterval(this,true)
                isLoop = SharePrefrenceValue().getClickStopType(this,true)
                if(isLoop == 2){
                    clickCycle = SharePrefrenceValue().getClickCycleValue(this,true)
                }

                println("## clickInterval $clickInterval")
                println("## isLoop $clickInterval")

                callTimer()
            }else{
              stopTimer()
            }

        }
        view.xImgSetting.setOnClickListener {
            if(!isStart)
                try{
                    SingleModeSettingDailog().showDialog(applicationContext,true)
                }catch (e : WindowManager.BadTokenException){
                    Toast.makeText(applicationContext,"try again something wrong",Toast.LENGTH_SHORT).show()
                }
                catch (e: Exception){
                             Toast.makeText(applicationContext,"try again something wrong",Toast.LENGTH_SHORT).show()
                }

        }
        view.xImgClose.setOnClickListener {
            var int = Intent()
            int.setAction("STOP")
            sendBroadcast(int)


            stopSelf()
        }

        clickView.setOnTouchListener(
            TouchAndDragListener(clickParam, startDragDistance,
                { noClick() },
                { manager.updateViewLayout(clickView, clickParam) },false)
        )

    }

    private fun stopTimer(){
        isStart = false
        view.xImgSetting.alpha = 1.0f
        view.xImgPlay.setImageDrawable(getDrawable(R.drawable.ic_play))
        freez = false
        timer!!.cancel()
    }
    private fun callTimer(){
        timer = object : CountDownTimer(clickInterval,1000){
            override fun onFinish() {
                callOneNumberClick()
            }

            override fun onTick(p0: Long) {

            }
        }
        isStart = true
        view.xImgSetting.alpha = 0.5f
        view.xImgPlay.setImageDrawable(getDrawable(R.drawable.ic_stop))
        freez = true
        timer!!.start()
    }
    private fun noClick(){}

    private fun viewOnClick() {
        if(isExpanded){
            isExpanded = false
            view.xImgPlay.visibility = View.GONE
            view.xImgSetting.visibility = View.GONE
            view.xImgClose.visibility = View.VISIBLE


        }else{
            isExpanded = true
            view.xImgPlay.visibility = View.VISIBLE
            view.xImgSetting.visibility = View.VISIBLE
            view.xImgClose.visibility = View.GONE
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        "FloatingClickService onDestroy".logd()
        if(timer != null){
            timer!!.cancel()
        }

        manager.removeView(view)
        manager.removeView(clickView)

    }
    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        "FloatingClickService onConfigurationChanged".logd()
        val x = params.x
        val y = params.y
        params.x = xForRecord
        params.y = yForRecord
        xForRecord = x
        yForRecord = y
        manager.updateViewLayout(view, params)
       // manager.updateViewLayout(clickView, params)


    }
}