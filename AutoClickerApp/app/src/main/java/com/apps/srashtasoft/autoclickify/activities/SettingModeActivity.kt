package com.apps.srashtasoft.autoclickify.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.apps.srashtasoft.autoclickify.R
import com.apps.srashtasoft.autoclickify.fragments.SingleTargetSettingFrg

class SettingModeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_mode)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if(intent.getBooleanExtra("IS_SINGLE_TARGET",true)){
            setTitle(R.string.strSingleTargetSetting)
        }else{
            setTitle(R.string.strMultiTargetSetting)
        }

        var frg = SingleTargetSettingFrg(intent.getBooleanExtra("IS_SINGLE_TARGET",true))
        var manager = supportFragmentManager.beginTransaction().replace(R.id.xFream,frg).commit()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{

                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
