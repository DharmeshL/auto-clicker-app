package com.apps.srashtasoft.autoclickify.services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.PixelFormat
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.view.*
import android.widget.*
import com.apps.srashtasoft.autoclickify.R
import com.apps.srashtasoft.autoclickify.dialogs.SingleModeSettingDailog
import com.apps.srashtasoft.autoclickify.extras.SharePrefrenceValue
import com.apps.srashtasoft.autoclickify.extras.Utils
import com.apps.srashtasoft.autoclickify.listners.TouchAndDragListener
import com.apps.srashtasoft.autoclickify.listners.freez

import dp2px
import kotlinx.android.synthetic.main.malti_target_clicks.view.*
import kotlinx.android.synthetic.main.malti_target_clicks.view.xImgClose
import kotlinx.android.synthetic.main.malti_target_clicks.view.xImgMove
import kotlinx.android.synthetic.main.malti_target_clicks.view.xImgSetting


import logd

class MaltiTargetModeService : Service() {

    private lateinit var manager: WindowManager
    private lateinit var view: View
    private lateinit var params: WindowManager.LayoutParams
    private var xForRecord = 0
    private var yForRecord = 0
    private val location = IntArray(2)
    private var startDragDistance: Int = 0
    private var isStart : Boolean = false
    private var isExpanded : Boolean = true
    private lateinit var clickParam: WindowManager.LayoutParams
    private var timer : CountDownTimer? = null
    private var clickCount : Int = 0
    private var imgArrayList = arrayListOf<RelativeLayout>()
    private var paramList = arrayListOf<WindowManager.LayoutParams>()
    private var clickInterval : Long =1000L
    private var isLoop : Int = 1
    private var clickCycle : Int = 1
    private var index : Int = 0;
    private var size : Int = 100;


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    private fun callClickFunction(){
        println("## callClickFunction Index is $index")
        imgArrayList.get(index).getLocationOnScreen(location)
        autoClickService?.click(location[0] + imgArrayList.get(index).right - 30,
            location[1] + imgArrayList.get(index).bottom  )

        timer!!.cancel()
        if(isLoop == 1){
            index++
            if(index == imgArrayList.size){
                index = 0
                timer!!.start()
            }else{
                timer!!.start()
            }
        }else{
            if(clickCount >  1){
                index++
                if(index == imgArrayList.size){
                    index = 0
                    timer!!.start()
                }  else{
                    timer!!.start()
                }
                clickCount--
            }else{
                stopTimer()
            }
        }

    }
    private fun setConrollerSize(cSize : Int){
        var xImgClose = view.findViewById(R.id.xImgClose) as ImageView
        var xImgPlay = view.findViewById(R.id.xImgPlayButton) as ImageView
        var xImgSetting = view.findViewById(R.id.xImgSetting) as ImageView
        var xImgMove = view.findViewById(R.id.xImgMove) as ImageView
        var xImgAddClick = view.findViewById(R.id.xImgAddClick) as ImageView
        var xImgRemoveClick = view.findViewById(R.id.xImgRemoveClick) as ImageView
        var scale : Float = this.resources.displayMetrics.density
        var pixel : Float = (cSize * scale + 0.5f)
        xImgClose.layoutParams.width = pixel.toInt()
        xImgClose.layoutParams.height = pixel.toInt()
        xImgPlay.layoutParams.width = pixel.toInt()
        xImgPlay.layoutParams.height = pixel.toInt()
        xImgSetting.layoutParams.width = pixel.toInt()
        xImgSetting.layoutParams.height = pixel.toInt()
        xImgMove.layoutParams.width = pixel.toInt()
        xImgMove.layoutParams.height = pixel.toInt()
        xImgRemoveClick.layoutParams.width = pixel.toInt()
        xImgRemoveClick.layoutParams.height = pixel.toInt()
        xImgAddClick.layoutParams.width = pixel.toInt()
        xImgAddClick.layoutParams.height = pixel.toInt()
    }

    override fun onCreate() {
        super.onCreate()
        startDragDistance = dp2px(10f)
        view = LayoutInflater.from(this).inflate(R.layout.malti_target_clicks, null)
         size = SharePrefrenceValue().getTargetValue(this)
        var Csize = SharePrefrenceValue().getControllerSize(this)
        if(Csize == 0){
            setConrollerSize(30)
        }else if(Csize == 1){
            setConrollerSize(40)
        }else{
            setConrollerSize(50)
        }
        val overlayParam =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
                WindowManager.LayoutParams.TYPE_PHONE
            }
        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
        params.gravity = Gravity.LEFT
        manager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        manager.addView(view, params)



        view.xImgMove.setOnTouchListener(
            TouchAndDragListener(params, startDragDistance,
                { viewOnClick() },
                { manager.updateViewLayout(view, params) },true)
        )
        view.xImgPlayButton.setOnClickListener {
            if(imgArrayList.size > 0){
                if(!isStart){
                    clickInterval = Utils().getClickInterval(this,false)
                    isLoop = SharePrefrenceValue().getClickStopType(this,false)
                    println("## Click Loop $isLoop")
                    if(isLoop == 2){
                        clickCycle = SharePrefrenceValue().getClickCycleValue(this,false)
                        println("## Click Cyle $clickCycle")
                        clickCount = clickCycle * imgArrayList.size
                        println("## Click Count $clickCount")
                    }
                   callTimer()

                }else{
                   stopTimer()
                }
            }else{
                Toast.makeText(this,this.resources.getString(R.string.alertMessage),Toast.LENGTH_LONG).show()
            }

        }
        view.xImgClose.setOnClickListener {
            var int = Intent()
            int.setAction("STOP_MULTI_TARGET")
            sendBroadcast(int)


            stopSelf()
        }
        view.xImgAddClick.setOnClickListener {
            if(!isStart)
            addClick(imgArrayList.size + 1)
        }
        view.xImgRemoveClick.setOnClickListener {
            if(!isStart)
            removeClick()
        }
        view.xImgSetting.setOnClickListener {
            if(!isStart)
                SingleModeSettingDailog().showDialog(this,false)
        }

    }

    private fun stopTimer(){
        timer!!.cancel()
        isStart = false
        view.xImgPlayButton.setImageDrawable(getDrawable(R.drawable.ic_play))
        view.xImgRemoveClick.alpha = 1.0f
        view.xImgAddClick.alpha = 1.0f
        view.xImgSetting.alpha = 1.0f
        freez = false
    }
    private fun callTimer(){
        index = 0
        timer = object : CountDownTimer(clickInterval,1000){
            override fun onFinish() {



                 callClickFunction()


            }

            override fun onTick(p0: Long) {

            }
        }

        timer!!.start()
        isStart = true
        view.xImgPlayButton.setImageDrawable(getDrawable(R.drawable.ic_stop))
        view.xImgRemoveClick.alpha = 0.5f
        view.xImgAddClick.alpha = 0.5f
        view.xImgSetting.alpha = 0.5f
        freez = true
    }

    private fun removeClick(){
        if(imgArrayList.size > 0)
        manager.removeView(imgArrayList.removeAt(imgArrayList.size - 1))
    }

    private fun addClick(id : Int){
        println(" ## Size $size")
        freez = false
        var relative = RelativeLayout(this)
        var rl = LinearLayout(this)

        var image = ImageView(this)
        var textView = TextView(this)

        var param = LinearLayout.LayoutParams(size,size)
        rl.layoutParams = param
        rl.setBackgroundColor(Color.BLUE)
        rl.setBackgroundResource(R.drawable.ic_target_324dp)
      //  param.gravity = Gravity.CENTER_HORIZONTAL
      //  relative.layoutParams = params

       relative.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)
    // var  params :RelativeLayout.LayoutParams = relative.layoutParams as RelativeLayout.LayoutParams
    //    params.addRule(RelativeLayout.CENTER_IN_PARENT)

       // relative.setBackgroundResource(R.drawable.ic_target)

   //   relative.setBackgroundResource(R.drawable.ic_target_324dp)
        //relative.setBackgroundColor(Color.CYAN)
        image.setImageDrawable(this.getDrawable(R.drawable.ic_target_324dp))
        image.layoutParams =LinearLayout.LayoutParams(size,size)
        rl.gravity = Gravity.CENTER
        textView.text = "$id"
        textView.gravity = Gravity.CENTER

     //   var textSize = 21f

        textView.textSize = Utils().getTextSize(size)

        textView.setTextColor(this.resources.getColor(R.color.colorAccent))
            rl.addView(textView)
     //   relative.addView(image)
        relative.addView(rl)

      //  relative.layoutParams = params


        val overlayParam =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
                WindowManager.LayoutParams.TYPE_PHONE
            }
        var clickParam: WindowManager.LayoutParams   = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayParam,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
        //image.layoutParams.width = 150
       // image.layoutParams.height = 150


        relative.setOnTouchListener(
            TouchAndDragListener(clickParam, startDragDistance,
                { nullClick() },
                { manager.updateViewLayout(relative, clickParam) },false)
        )

        imgArrayList.add(relative)
        manager.addView(relative,clickParam)
    }
    private fun nullClick(){

    }
    private fun viewOnClick(){
        if(isExpanded){
            isExpanded = false
            view.xImgSetting.visibility = View.GONE
            view.xImgAddClick.visibility = View.GONE
            view.xImgRemoveClick.visibility = View.GONE
            view.xImgPlayButton.visibility = View.GONE
            view.xImgClose.visibility = View.VISIBLE
        }else{
            isExpanded = true
            view.xImgSetting.visibility = View.VISIBLE
            view.xImgAddClick.visibility = View.VISIBLE
            view.xImgRemoveClick.visibility = View.VISIBLE
            view.xImgPlayButton.visibility = View.VISIBLE
            view.xImgClose.visibility = View.GONE
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        "FloatingClickService onDestroy".logd()
        if(timer != null){
            timer!!.cancel()
        }


        manager.removeView(view)

        if(imgArrayList.size > 0){
            imgArrayList.forEach {
                manager.removeView(it)
            }
        }


    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        "FloatingClickService onConfigurationChanged".logd()
        val x = params.x
        val y = params.y
        params.x = xForRecord
        params.y = yForRecord
        xForRecord = x
        yForRecord = y
        manager.updateViewLayout(view, params)



    }

    //click 392 696
}