package com.apps.srashtasoft.autoclickify.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.apps.srashtasoft.autoclickify.R

class FragmentDialog : DialogFragment() {


    companion object {


        /**
         * Create a new instance of CustomDialogFragment, providing "num" as an
         * argument.
         */
        fun newInstance(content: String): FragmentDialog {
            val f = FragmentDialog()

            // Supply num input as an argument.
            val args = Bundle()
            args.putString("content", content)
            f.arguments = args

            return f
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Pick a style based on the num.
        //val style = DialogFragment.STYLE_NO_FRAME
        //val theme = R.style.DialogTheme
        //setStyle(style, theme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.single_target_setting_frg, container, false)

        return view
    }
}