package com.apps.srashtasoft.gameworldapp;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

public class ExitAppDialog {
    private AlertDialog alertDialog;
    AlertDialog alertOpenReview ;
    public  void showDialog(final Context context){

        LayoutInflater inflater =  LayoutInflater.from(context);
        View alertLayout = inflater.inflate(R.layout.dialog_alert, null);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setView(alertLayout);
        builder1.setCancelable(true);
        final TextView xTvCancel = (TextView) alertLayout.findViewById(R.id.txt_no);
        final TextView txt_yes = (TextView) alertLayout.findViewById(R.id.txt_yes);
        RatingBar ratingBar = (RatingBar) alertLayout.findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    rateUS(context);
            }
        });
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).finishScreen();

                    alertOpenReview.dismiss();


            }
        });

        xTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertOpenReview.dismiss();
            }
        });

        alertOpenReview = builder1.create();
        alertOpenReview.show();

       /* AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_alert, null);
        alertDialog = builder.setView(dialogView).setCancelable(false)
                .create();
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView xTvExit = (TextView)dialogView.findViewById(R.id.xTvExit);
        TextView xTvMoreApp = (TextView)dialogView.findViewById(R.id.xTvMoreApp);
        TextView xTvCancel = (TextView)dialogView.findViewById(R.id.xTvCancel);
        xTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        xTvExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).finishScreen();
                alertDialog.dismiss();
            }
        });

        xTvMoreApp.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Uri uri =  Uri.parse("market://search?q=srashtasoft");
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try{
                            context.startActivity(goToMarket);
                        }catch (Exception e){

                            context.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/search?q=srashtasoft&hl=en")));
                        }
                    }
                }
        );*/



      //  alertDialog.show();
    }

    private void rateUS(Context context){

        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id="+context.getPackageName()+"&hl=en");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }
}
