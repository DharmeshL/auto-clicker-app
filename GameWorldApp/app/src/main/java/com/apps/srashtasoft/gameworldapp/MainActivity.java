package com.apps.srashtasoft.gameworldapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.os.BuildCompat;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.firebase.analytics.FirebaseAnalytics;


public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
    private static FirebaseAnalytics firebaseAnalytics;
    private WebView xWebview;
    private ProgressBar xHorizontalPg;
    private LinearLayout xLLNoInternateView,xLLWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.app_name);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        xWebview = (WebView)findViewById(R.id.xWebview);
        xHorizontalPg = (ProgressBar)findViewById(R.id.xHorizontalPg) ;
        xLLNoInternateView = (LinearLayout)findViewById(R.id.xLLNoInternateView);
        xLLWebView = (LinearLayout)findViewById(R.id.xLLWebView);
        if(hasInternateConnnect(this)){
            xLLNoInternateView.setVisibility(View.GONE);
            xLLWebView.setVisibility(View.VISIBLE);
            setWebview(BuildConfig.BASE_URL);

        }else{
            xLLNoInternateView.setVisibility(View.VISIBLE);
            xLLWebView.setVisibility(View.GONE);
        }


    }

    private void setWebview(String url){
        xWebview.getSettings().setJavaScriptEnabled(true);
        xWebview.getSettings().setLoadsImagesAutomatically(true);
        xWebview.setInitialScale(1);
        xWebview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        xWebview.getSettings().setLoadWithOverviewMode(true);
        xWebview.getSettings().setUseWideViewPort(true);
        xWebview.getSettings().setBuiltInZoomControls(false);
        xWebview.getSettings().setDisplayZoomControls(false);
        xWebview.getSettings().setDomStorageEnabled(true);
        xWebview.getSettings().setAllowFileAccess(true);
        xWebview.getSettings().setSavePassword(true);
        xWebview.getSettings().setGeolocationEnabled(true);
        xWebview.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                xWebview.loadUrl(request.getUrl().toString());

                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                xHorizontalPg.setVisibility(View.GONE);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                xHorizontalPg.setVisibility(View.VISIBLE);

            }
        });
        xWebview.loadUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_rate_app:
                Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
                }
                return true;
            case R.id.nav_share:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                    String shareMessage= "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (xWebview.canGoBack()) {
            xWebview.goBack();
        }else{
            new ExitAppDialog().showDialog(this);
            // finish();
        }
    }
    public void finishScreen(){
        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() != KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {

        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    public static boolean hasInternateConnnect(Context context){
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try{
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                Network network = connectivityManager.getActiveNetwork();

                NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(network);
                if(capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) == true ||
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) == true
                ){
                    isConnected = true;
                }else{
                    isConnected = false;
                }



            }else{
                final android.net.NetworkInfo wifi = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                final android.net.NetworkInfo mobile = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                if (wifi.isAvailable() || mobile.isAvailable()) {
                    isConnected = true;
                }else{
                    isConnected = false;
                }


            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return isConnected;
    }
}
